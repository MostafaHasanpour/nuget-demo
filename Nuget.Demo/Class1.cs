﻿namespace Nuget.Demo
{
    public static class Class1
    {
        public static void SayHello(this string name)
        {
            Console.WriteLine($"{name}: Hello");
        }
    }
}